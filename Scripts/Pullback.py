#+--------------------------------------+
#|                    Pullback Approach |
#!          Copyright 2018, AmadeusAlgo |
#|                     Version 0.4 beta |
#+--------------------------------------+

#+--------------------------------------+
#| Library Setup                        |
#+--------------------------------------+

from time import time, sleep
import numpy as np
import pandas as pd

# Suppresses SettingWithCopyWarning in Pandas
pd.options.mode.chained_assignment = None  # default='warn'

from oandapyV20 import API
from oandapyV20.endpoints import accounts, instruments, orders, positions
from oandapyV20.endpoints import pricing, responses, trades, transactions
from oandapyV20.contrib.requests import MITOrderRequest, StopLossDetails
from oandapyV20.contrib.requests import StopLossOrderRequest

from Live.access import access_oanda_prac, access_oanda_prac_acct
from Live.access import access_oanda, access_oanda_acct

import ohlcv as oh
import indicators as ind
import parameters as par
#import endpoints as en #Not implemented

now = lambda: int(time())

#+--------------------------------------+
#| Algo initialization function         |
#+--------------------------------------+

def OnInit():

    # Initialise
    print('Initialising Pullback script...\n')
    global startTime
    startTime = pd.datetime.now()

    # Initialise the API
    global oanda, accountID, orderID
    if par.useDemoAPI:
        oanda = API(access_token=access_oanda_prac())
        accountID = access_oanda_prac_acct()
    else:
        oanda = API(access_token=access_oanda(), environment='live')
        accountID = access_oanda_acct()

    orderID = None # Placeholder value
    print('Using account {}...'.format(accountID))

    # Initialise OHLCV data
    print('Loading OHLCV...')
    OHLCV = loadInitOHLCV()
    print('Loaded OHLCV of size {}.'.format(OHLCV.shape[0]))

    ## Set last buy (sell) stop price when MACD_Hist < 0 (> 0)
    ## BuyStop = Big DC High + Pip_diff if MACD_Hist > 0 else BuyStop
    ## SellStop = Big DC Low - Pip_diff if MACD_Hist > 0 else SellStop
    global BuyStop, SellStop
    BuyStop, SellStop = getFirstStop(OHLCV)
    print('First buystop and sellstop prices loaded at {} and {}.'.format(
        BuyStop, SellStop
    ))

    # Previous versions set this flag to True. Now it's set to false for
    # faster trade acquisition
    global MACDFlag
    MACDFlag = False
    print('MACDFlag is set to {} in OnInit.'.format(MACDFlag))

    return None

#+--------------------------------------+
#| Algo deinitialization function       |
#+--------------------------------------+

def OnDeInit():

    # TODO: Get the transaction data from startDate set in OnInit
    # Prerequisites: transaction.py completed

    # End the script with this
    print('\nEnding Pullback script.')

    return None

#+--------------------------------------+
#| Algo interval function               |
#+--------------------------------------+


def OnInterval():

    print('Time now is {}.'.format(pd.datetime.now()))

    global OHLCV, BuyStop, SellStop, MACDFlag

    # Update data, OHLC, buy & sell stop prices
    print('Loading trade data and OHLCV from Oanda...')
    oandaData = loadOandaData()
    OHLCV = loadOHLCV()
    print('Loaded, last available OHLC time is {}.'.format(OHLCV.index[-1]))

    # Initialise Indicators
    indicators = setIndicators(OHLCV)
    BuyStop, SellStop = updateStop(indicators, BuyStop, SellStop)
    print('Loaded indicators and BuyStop.\n'
          'Indicators: {}\n'
          'BuyStop: {}, SellStop: {}'.format(indicators,
                                             BuyStop, SellStop))

    # Remove flag if MACD region is new
    removeMACDFlag(indicators['MACD_Hist'], indicators['MACD_Hist2'])


    # As a whole:
    # 1. Delete
    #   - If there is a pending trade, Check if conditions are still good,
    #      otherwise delete the trade, delete from tradeData as well
    #   - Closed trades:
    #     + Update Closed status, Reversal and AntiMart values on closed trades
    # 2. Modify
    #   - Running trades:
    #     + Check if SL needs to change
    #   - Pending trades:
    #     + Check if require to change price/SL/lot size
    # 3. Open
    #   - Check for conditions before opening pending trade

    # Delete
    deleteTrades(indicators, oandaData)
    # Reloading Oanda Data
    oandaData = loadOandaData()

    # Modify
    modifyTrades(indicators, oandaData)

    # Open
    if (MACDFlag == False) and (oh.executionTime()): openTrades(indicators, oandaData)


    print('Time ended is {}. Sleeping...'.format(pd.datetime.now()))
    return None

#+--------------------------------------+
#| Main Delete, Modify and Open         |
#+--------------------------------------+

def deleteTrades(indic, oandaData):
    '''
    If there is a pending trade, Check if conditions are still good,
    otherwise delete the trade
    '''

    global MACDFlag

    # Read the oandaData and filter for STOPS and LIMITS
    orderData = oandaData[oandaData.Type.isin(['LIMIT', 'STOP',
                                               'MARKET_IF_TOUCHED'])]
    print('Current orderIDs: {}.'.format(list(orderData.index)))

    for orderID in orderData.index:
        delCond = False
        # Check for their unit sign (type-wise should be STOP only)
        # If stop order is a sell...
        if orderData.loc[orderID, 'Units'] < 0:
            # Check whether sell conditions are invalid:
            # (SMA_Diff > 0) | (Don_S_U > Don_L_M)
            if (indic['SMA_Diff'] > 0) | \
               (indic['Don_S_U'] > indic['Don_L_M']):
                delCond = True

        # Else (means that it is a buy)...
        else:
            # Check whether buy conditions are invalid:
            if (indic['SMA_Diff'] < 0) | \
               (indic['Don_S_L'] < indic['Don_L_M']):
                delCond = True

        if delCond:
            # If it is the case, then close the current order
            # and drop the orderID in oandaData
            OrderClose(orderID)
            oandaData.drop(index=orderID)
            print('Closed orderID {} due to invalid '
                  'conditions.'.format(orderID))
            MACDFlag = False
            print('MACDFlag is set to {} in deleteTrades.'.format(MACDFlag))

    # Return case placeholder
    return None

def modifyTrades(indic, oandaData):
    '''
    For running trades, check whether SL needs to change.

    For pending trades, check whether price/SL/lotsize needs to change.

    To do both, best to iterate tradeData[tradeData.Closed ==
    False].index and check tradeID status from both tradeData
    and oandaData.
    '''

    global BuyStop, SellStop, accountID, oanda

    # Read the oandaData and filter for STOPS and LIMITS
    orderData = oandaData[oandaData.Type.isin(['LIMIT', 'STOP',
                                               'MARKET_IF_TOUCHED'])]
    openedTrades = oandaData[oandaData.Type.isin(['MARKET'])]

    # For pending trade
    for orderID in orderData.index:

        # Calculate the supposed price, SL and lotsize that it needs
        # BuyStop/SellStop is the price
        if orderData.loc[orderID, 'Units'] > 0: # If it's long
            SL = indic['Don_SX_L'] - par.Pip_Diff_SL*(10**-(decPnt-1))
            StopOrder = BuyStop
        else: # If it's short
            SL = indic['Don_SX_U'] + par.Pip_Diff_SL*(10**-(decPnt-1))
            StopOrder = SellStop

        SL = round(SL, 5)

        riskPrev = 0
        # MicroHEAT Toggle
        if par.microHEAT & (openedTrades.shape[0] > 0):
            riskPrev = max(round(((openedTrades.Price - openedTrades.SL)*openedTrades.Units*(10**-(5-decPnt))).sum(), 2), 0)

        orderUnits = calcLotSize(max(par.risk - riskPrev, 0.01), round((StopOrder - SL)*(10**-(5-decPnt)), 5))

        if abs(orderUnits) < 1:
            if orderUnits < 0: orderUnits = -1
            else: orderUnits = 1

        realUnits = int(orderUnits)
        orderDetails = {'Price': round(orderData.loc[orderID, 'Price'], decPnt),
                        'SL': round(orderData.loc[orderID, 'SL'], decPnt),
                        'Units': int(orderData.loc[orderID, 'Units'])}
        # If the values are different, do an OrderReplace
        if ((StopOrder != orderDetails['Price']) or \
            (SL != orderDetails['SL']) or \
            (realUnits != orderDetails['Units'])):
            setOrder = MITOrderRequest(
                instrument=par.instrument,
                units = realUnits,
                price = StopOrder,
                stopLossOnFill=StopLossDetails(price=SL).data
            ).data
            r = orders.OrderReplace(accountID=accountID,
                                    orderID=orderID,
                                    data=setOrder)
            rv = oanda.request(r)
            print('Changed orderID {} to {}. \n'
                  'Price: {} -> {} \n'
                  'SL:    {} -> {} \n'
                  'Units: {} -> {}.'.format(
                      orderID, rv['orderCreateTransaction']['id'],
                      orderDetails['Price'], BuyStop,
                      orderDetails['SL'], SL,
                      orderDetails['Units'], realUnits,
                  ))

    # For running trades
    market_data = oandaData[oandaData.Type.isin(['MARKET'])]

    for tradeID in market_data.index:
        currentSL = round(market_data.loc[tradeID, 'SL'], decPnt)
        if market_data.loc[tradeID, 'Units'] > 0: # Long
            SL = round(indic['Don_SX_L'] - par.Pip_Diff_SL*(10**-(decPnt-1)), decPnt)
            cond = SL > currentSL
        else: # Short
            SL = round(indic['Don_SX_U'] + par.Pip_Diff_SL*(10**-(decPnt-1)), decPnt)
            cond = SL < currentSL

        if cond:
            setOrder = StopLossOrderRequest(tradeID=tradeID, price=SL).data
            SLOrderID = GetSLOrderID(tradeID)
            r = orders.OrderReplace(accountID=accountID, orderID=SLOrderID,
                                    data=setOrder)
            rv = oanda.request(r)
            print('Changed SL from {} to {} for '
                  'tradeID {}.'.format(currentSL, SL, tradeID))

    return None

def openTrades(indic, oandaData):
    '''
    Check for conditions before opening trade.

    Note that while you execute the trade in oanda with Reversal and
    AntiMart in mind, only store 'normal' trades in tradeData so that
    equity curve manipulation can be done as intended.
    '''

    global orderUnits, BuyStop, SellStop, MACDFlag
    global accountID, oanda

    # Check how many trades there are
    orderData = oandaData[oandaData.Type.isin(['LIMIT', 'STOP',
                                               'MARKET_IF_TOUCHED'])]
    openedTrades = oandaData[oandaData.Type.isin(['MARKET'])]

    # For pending trade
    if ((openedTrades.shape[0] < par.stackNumber) & (orderData.shape[0] < 1)):
        createOrder = False
        if   ((indic['SMA_Diff'] > 0) & \
             (indic['Don_S_L'] > indic['Don_L_M'])) & \
             (openedTrades.empty or \
             (openedTrades.Price.iloc[-1] <= BuyStop - 5*(10**-(decPnt-1)))): # Long
            Price = BuyStop
            SL = round(indic['Don_SX_L'] - par.Pip_Diff_SL*(10**-(decPnt-1)), decPnt)

            riskPrev = 0
            # MicroHEAT Toggle
            if par.microHEAT & (openedTrades.shape[0] > 0):
                riskPrev = max(round(((openedTrades.Price - openedTrades.SL)*openedTrades.Units*(10**-(5-decPnt))).sum(), 2), 0)

            orderUnits = max(calcLotSize(max(par.risk - riskPrev, 0.01), (Price - SL)*(10**-(5-decPnt))), 1)
            createOrder = True
        elif (indic['SMA_Diff'] < 0) & \
             (indic['Don_S_U'] < indic['Don_L_M']) & \
             (openedTrades.empty or \
             (openedTrades.Price.iloc[-1] >= SellStop + 5*(10**-(decPnt-1)))): # Short
            Price = SellStop
            SL = round(indic['Don_SX_U'] + par.Pip_Diff_SL*(10**-(decPnt-1)), decPnt)

            riskPrev = 0
            # MicroHEAT Toggle
            if par.microHEAT & (openedTrades.shape[0] > 0):
                riskPrev = max(round(((openedTrades.Price - openedTrades.SL)*openedTrades.Units*(10**-(5-decPnt))).sum(), 2), 0)

            orderUnits = min(calcLotSize(max(par.risk - riskPrev, 0.01), (Price - SL)*(10**-(5-decPnt))), -1)
            createOrder = True

        if createOrder:
            print('Creating order with {} Price, {} SL'.format(Price, SL))
            realUnits = int(orderUnits)
            setOrder = MITOrderRequest(
                instrument=par.instrument,
                units = realUnits,
                price = Price,
                stopLossOnFill=StopLossDetails(price=SL).data
            ).data
            r = orders.OrderCreate(accountID=accountID, data=setOrder)
            rv = oanda.request(r)
            orderID = int(rv['orderCreateTransaction']['id'])
            print('Created orderID {}. \n'
                  'Price is {}.'.format(
                      orderID, Price
                  ))

            MACDFlag = True
            print('MACDFlag is set to {} in openTrades.'.format(MACDFlag))


    return None

#+--------------------------------------+

#+--------------------------------------+
#| Main Functions in OnInit             |
#+--------------------------------------+

# For simulated trades
def loadInitOHLCV():
    return loadOHLCV()

## Set last buy (sell) stop price when MACDHist < 0 (> 0)
## BuyStop = Big DC High + Pip_diff if MACDHist > 0 else BuyStop
## SellStop = Big DC Low - Pip_diff if MACDHist > 0 else SellStop
def getFirstStop(OHLCV):
    df = OHLCV[-100:]
    df['MACD_Hist'] = ind.MACDHist(OHLCV.close, *par.MACD_param)
    df['Don_S_U'] = ind.DonchianH(OHLCV.high, par.Don_S)
    df['Don_S_L'] = ind.DonchianL(OHLCV.low, par.Don_S)
    df['BuyStop'] = np.where(df.MACD_Hist.shift(1) < 0,
                             df.Don_S_U.shift(1) + \
                             par.Pip_Diff*(10**-(decPnt-1)), None)
    df['BuyStop'].fillna(method='ffill', inplace=True)
    df['SellStop'] = np.where(df.MACD_Hist.shift(1) > 0,
                              df.Don_S_L.shift(1) - \
                              par.Pip_Diff*(10**-(decPnt-1)), None)
    df['SellStop'].fillna(method='ffill', inplace=True)
    return round(df.BuyStop.iloc[-1], decPnt), \
           round(df.SellStop.iloc[-1], decPnt)

#+--------------------------------------+
#| Main Functions in OnInterval         |
#+--------------------------------------+

def loadOandaData():
    '''
    It stores the following info:
    Time, Symbol, Type, Price, SL, Units, Closed, PnL

    - tradeID: Index using Int64, applicable to all trades (unique)
      + Note that ID will always change for pending trades when any price,
        SL and lotsize changes, will stay fixed once the trade is opened
      + Originally `id` for all
    - Time: Datetime64, applicable to all pending and running trades
      + Originally `createTime` for pending, `openTime` for running
    - Symbol: Str, applicable to all trades
      + Originally `instrument` for all
    - Type: Str, applicable to all trades,
      + STOP/LIMIT for pending, MARKET for running
      + Originally `type` for pending, added for running to be compatible
        with orders
    - Price: Double, applicable to all trades
      + Originally `price` for all
    - SL: Double, applicable to all trades
      + Will be denoted as NaN if there is no SL
      + Originally `stopLossOnFill` for pending, `stopLossOrder` for
        running
    - Units: Int, applicable to all trades
      + Positive for long, negative for short
      + Originally `units` for all
    - Closed: Boolean, applicable to all trades
      + Added for oanda data to be compatible with other data processes
      + All are False
    - PnL: Double, applicable to running trades only
      + For pending trades, they should be NaN
      + Originally `unrealizedPL` for running trades
  '''

    global accountID

    # Looking at Pending Orders (Limits, Stops, SLs, TPs)
    r = orders.OrderList(accountID=accountID, params = {"instrument": par.instrument})
    rv = oanda.request(r)

    # Looking at Running Trades
    r2 = trades.TradesList(accountID=accountID, params = {"instrument": par.instrument})
    rv2 = oanda.request(r2)

    res = pd.DataFrame(columns=['Time', 'Symbol', 'Type', 'Price', 'SL',
                                'Units', 'Closed', 'PnL'])
    res.index.rename('tradeID', inplace=True)

    if rv['orders'] != []:
        # For pending trades
        orderData = pd.DataFrame(rv['orders'])

        # Filter suitable trades for orderData
        orderData = orderData[orderData.type.isin(['LIMIT', 'STOP',
                                                   'MARKET_IF_TOUCHED'])]

        if orderData.empty == False:
            # Reorder and rename the data
            # [tradeID, Time, Symbol, Type, Price, SL, Units, Closed]

            orderData.set_index('id', inplace=True)
            orderData.index.rename('tradeID', inplace=True)
            orderData['Closed'] = False
            if 'stopLossOnFill' not in orderData.columns:
                orderData['stopLossOnFill'] = np.NaN
            orderData = orderData[['createTime', 'instrument', 'type', 'price',
                                   'stopLossOnFill', 'units', 'Closed']]
            orderData.columns = ['Time', 'Symbol', 'Type', 'Price', 'SL',
                                 'Units', 'Closed']
            try: orderData.loc[:,'SL'] = orderData.SL.apply(pd.Series)['price']
            except: pass
            orderData.loc[:,'Time'] = pd.to_datetime(orderData.Time)

            res = res.append([orderData], sort=False)

    # For running trades
    if rv2['trades'] != []:
        tradeData = pd.DataFrame(rv2['trades'])

        # Reorder and rename the data
        # [tradeID, Time, Symbol, Type, Price, SL, Units, Closed, PnL]

        tradeData.set_index('id', inplace=True)
        tradeData.index.rename('tradeID', inplace=True)
        tradeData['Closed'] = False
        tradeData['type'] = 'MARKET'
        if 'stopLossOrder' not in tradeData.columns:
            tradeData['stopLossOrder'] = np.NaN
        tradeData = tradeData[['openTime', 'instrument', 'type', 'price',
                               'stopLossOrder', 'currentUnits', 'Closed',
                               'unrealizedPL']]
        tradeData.columns = ['Time', 'Symbol', 'Type', 'Price', 'SL',
                             'Units', 'Closed', 'PnL']
        try: tradeData.loc[:,'SL'] = tradeData.SL.apply(pd.Series)['price']
        except: pass
        tradeData.loc[:,'Time'] = pd.to_datetime(tradeData.Time)

        res = res.append([tradeData], sort=False)

    res.index = pd.to_numeric(res.index)
    res.loc[:, ['Price', 'SL', 'Units']] = \
        res[['Price', 'SL', 'Units']].apply(pd.to_numeric)
    res = res.sort_index()

    return res

# For indicators
def loadOHLCV():
    global oanda
    return oh.collectOHLCV(oanda, par.instrument, count=520,
                        granularity=par.timeFrame)

def setIndicators(OHLCV):
    indicators = {}

    ## Big DC High & Low
    indicators['Don_S_U'] = round(ind.DonchianH(OHLCV.high, par.Don_S)[-1], decPnt)
    indicators['Don_S_L'] = round(ind.DonchianL(OHLCV.low, par.Don_S)[-1], decPnt)
    ## Small DC High & Low
    indicators['Don_SX_U'] = round(ind.DonchianH(OHLCV.high, par.Don_SX)[-1], decPnt)
    indicators['Don_SX_L'] = round(ind.DonchianL(OHLCV.low, par.Don_SX)[-1], decPnt)
    ## Huge DC Middle
    indicators['Don_L_M'] = round((lambda x, y: (x+y)/2)(
        ind.DonchianH(OHLCV.high, par.Don_L)[-1],
        ind.DonchianL(OHLCV.low, par.Don_L)[-1]
    ), decPnt)
    ## MACD Histogram
    indicators['MACD_Hist'] = round(ind.MACDHist(OHLCV.close,
                                                 *par.MACD_param)[-1], decPnt)
    indicators['MACD_Hist2'] = round(ind.MACDHist(OHLCV.close,
                                                  *par.MACD_param)[-2], decPnt)
    ## Moving Average Crossover
    indicators['SMA_Diff'] = round(ind.MACross(OHLCV.close,
                                                par.SMA_Fast, par.SMA_Slow)[-1], decPnt)

    return indicators

def updateStop(indic, BS, SS):
    '''
    Updates the BuyStop and SellStop values.
    '''
    # Get MACD_Hist
    MACD = indic['MACD_Hist']
    # Change values according to the MACD
    if MACD > 0: SS = indic['Don_S_L'] - par.Pip_Diff*(10**-(decPnt-1))
    elif MACD < 0: BS = indic['Don_S_U'] + par.Pip_Diff*(10**-(decPnt-1))

    return round(BS, decPnt), round(SS, decPnt)

decPnt = 3 if "JPY" in par.instrument else 5

# Simplified, need to change for cross pairs
# TODO: To fix the problem in an elegant way, no force of minimum SL distance
def calcLotSize(risk, SLpips):
    return int(risk/SLpips)

# Remove MACD Flag if region changes
def removeMACDFlag(macd, macd2):
    global MACDFlag

    if ((macd2 > 0) & (macd < 0)):
        MACDFlag = False
        print('MACDFlag is set to '
              '{} for removeMACDFlag.'.format(MACDFlag))

    return None

#+--------------------------------------+
#| Deleting Orders Subfunctions         |
#+--------------------------------------+

# TODO: Add error exception like MT4
def OrderClose(orderID):
    global accountID, oanda

    r = orders.OrderCancel(accountID=accountID,
                           orderID=orderID)
    rv = oanda.request(r)
    print('Order number {} has been canceled at {}.'.format(
        orderID, rv['orderCancelTransaction']['time']
    ))
    return None

#+--------------------------------------+
#| Modify Orders Subfunctions           |
#+--------------------------------------+

def GetSLOrderID(tradeID):
    global accountID

    r = trades.TradeDetails(accountID=accountID, tradeID=tradeID)
    rv = oanda.request(r)

    return rv['trade']['stopLossOrder']['id']

#+--------------------------------------+
#| Main Function Subfunctions           |
#+--------------------------------------+

def sleepTime(interval, timeNow, timeLeft):
    getTime = interval - (time() - timeLeft) - 0.5
    if getTime > 0:
        return getTime
    else:
        return 0

def exceptionHandler(delay):
    import mail, sys, traceback
    from Live.access import errorMailHandler, passwdMailHandler, smtpMailHandler, errorMailCatcher

    print("Running exceptionHandler")

    # Get Traceback details
    exc_type, exc_value, exc_traceback = sys.exc_info()
    lines = traceback.format_exception(exc_type, exc_value, exc_traceback)
    lines = '\n'.join([line for line in lines])

    # Mail those details
    subject = "There is an error in the Pullback script.\n\n" + \
              "Script Tag: {}\n\n".format(par.scriptTitle) + \
              "Error:\n{}".format(lines)
    msg = mail.msg(errorMailHandler, [errorMailCatcher], 'Pullback Error', subject)
    mail.sendmail(msg, passwdMailHandler, smtpMailHandler)

    # Sleep for X seconds before continuing
    delay = 60 * 2 ** delay
    print('Resting for {} seconds...'.format(delay))
    sleep(delay)

    return None

#+--------------------------------------+

# Define on which interval should the time be running at
# Hourly = 3600
# Every minute = 60
# Every second = 1
interval = 60

if __name__ == "__main__":
    OnInit()

    exceptionDelay = 0

    while True:
        try:
            while True:
                while oh.marketTime():
                    if (now() % interval) == 7: # Lag by 7 seconds to get accurate OHLC data
                        timeLeft = time()
                        OnInterval()
                        exceptionDelay = 0
                        sleep(sleepTime(interval, time(), timeLeft))
                oh.sleepMarketTime()
        except KeyboardInterrupt:
            print("Interrupted by keyboard cancel.")
            break
        except Exception as e:
            try:
                exceptionHandler(exceptionDelay)
                exceptionDelay += 1
            except:
                print("Interrupted by exceptionHandler error.")
                break

    OnDeInit()
