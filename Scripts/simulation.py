# To store: [tradeID, Time, Symbol, Type, Price, SL, 
#            Units, Closed, P&L, Reversal, AntiMart]

import pandas as pd, numpy as np

from indicators import *
from parameters import *
from func import ConLoss

# Suppresses SettingWithCopyWarning in Pandas
pd.options.mode.chained_assignment = None  # default='warn'

def simulated_trades(ohlcv, numTrades=50):
    
    halfspr = 0.00007
    
    # Indicator setup
    indic = pd.DataFrame()

    indic['Don_S_H'] = Donchian_H(ohlcv.high, Don_SX)
    indic['Don_S_L'] = Donchian_L(ohlcv.low, Don_SX)
    indic['Don_L_H'] = Donchian_H(ohlcv.high, Don_S)
    indic['Don_L_L'] = Donchian_L(ohlcv.low, Don_S)
    indic['Don_H_M'] = (lambda x, y: (x+y)/2)(
        Donchian_H(ohlcv.high, Don_L),
        Donchian_L(ohlcv.low, Don_L)
    )

    indic['MACD_Hist'] = MACD_Hist(ohlcv.close, *MACD_param)
    indic['SMA_Diff']  = MA_Cross(ohlcv.close, SMA_Fast, SMA_Slow)

    indic['Buy_Chan']  = indic.Don_L_L > indic.Don_H_M 
    indic['Sell_Chan'] = indic.Don_L_H < indic.Don_H_M 
    #+--------------------------------------+
    
    # Long Signal
    longSig = pd.DataFrame(index=ohlcv.index)

    longSig['Stop'] = np.where(indic.MACD_Hist.shift(1) < 0,
                               indic.Don_L_H.shift(1) + Pip_Diff*0.0001,
                               None)
    longSig['Stop'].fillna(method='ffill', inplace = True)

    longSig['Sig'] = np.where((ohlcv.high + halfspr) > longSig.Stop, 1, 0)
    longSig['Sig'] = np.where(longSig.Stop > ohlcv.open, longSig.Sig, 0)
    longSig['Sig'] = np.where(indic.SMA_Diff.shift(1) > 0, longSig.Sig, 0)
    longSig['Sig'] = np.where(indic.Buy_Chan.shift(1), longSig.Sig, 0)

    longSig['InitialSL'] = indic.Don_S_L.shift(1) - Pip_Diff_SL*0.0001
    longSig['InitialSL'] = np.where((longSig.Stop - longSig.InitialSL) > \
                                    Pip_Diff_SL*0.0001,
                                    longSig.InitialSL, 
                                    longSig.Stop - \
                                    Pip_Diff_SL*0.0001)
    
    # MACD Pullback region
    longSig['macd'] = np.where((indic.MACD_Hist.shift(2) > 0) & \
                               (indic.MACD_Hist.shift(1) < 0), 1, 0)
    longSig['macd'] = longSig['macd'].cumsum()

    # Unstacking such that limit to one trade per macd region
    longSig.loc[longSig.Sig == 0, 'Sig'] = None
    longSig['Sig'] = np.where(longSig['macd'].shift(1) == longSig['macd'], 
                              np.where(longSig['Sig'].shift(1) == 1,None, 
                                       longSig['Sig']),
                             np.where(longSig['Sig'] == 1,
                                 longSig['Sig'], 0))
    
    longSig['Sig'].fillna(method='ffill', inplace=True)
    
    longSig['Sig'] = np.where(longSig['Sig'].shift(1) + longSig['Sig'] == 2 ,0, longSig['Sig'])
    
    longSig['SigPrice'] = np.where(longSig.Sig == 1, longSig.Stop, 0)

    longSig['StopLoss'] = np.where((ohlcv.low + halfspr) < longSig.InitialSL,
                                   longSig.InitialSL, None)
    longSig['StopLoss'].fillna(method='bfill', inplace=True)
    
    longSig['Closed'] = np.where(longSig.StopLoss == None, False, True)
    
    # Replaced Close Price to current InitialSL
    longSig['StopLoss'].fillna(value=longSig.InitialSL.iloc[-1], 
                               inplace=True)
    longSig['StopLoss'] = np.where(longSig.InitialSL > longSig.StopLoss,
                                   longSig.InitialSL,  longSig.StopLoss)
    
    # Trade Duration
    longSig['SLtime'] = pd.to_datetime(
        np.where((ohlcv.low + halfspr) < longSig.InitialSL, 
                 longSig.index, None)
    )
    longSig['SLtime'].fillna(method='bfill', inplace=True)
    longSig['SLtime'].fillna(value=longSig.index[-1], inplace=True)
    longSig['SLtime'] = np.where(longSig.InitialSL > longSig.StopLoss,
                                 longSig.index,      longSig.SLtime)
    #+--------------------------------------+
    
    # Short Signal
    shortSig = pd.DataFrame(index=ohlcv.index)

    shortSig['Stop'] = np.where(indic.MACD_Hist.shift(1) > 0,
                                indic.Don_L_L.shift(1) - Pip_Diff*0.0001, 
                                None)
    shortSig['Stop'].fillna(method='ffill', inplace = True)

    shortSig['Sig'] = np.where((ohlcv.low - halfspr) < shortSig.Stop, 1, 0)
    shortSig['Sig'] = np.where(shortSig.Stop < ohlcv.open, shortSig.Sig, 0)
    shortSig['Sig'] = np.where(indic.SMA_Diff.shift(1) < 0, shortSig.Sig, 0)
    shortSig['Sig'] = np.where(indic.Sell_Chan.shift(1), shortSig.Sig, 0)

    shortSig['InitialSL'] = indic.Don_S_H.shift(1) + Pip_Diff_SL*0.0001
    shortSig['InitialSL'] = np.where((shortSig.InitialSL - shortSig.Stop) > \
                                     Pip_Diff_SL*0.0001,
                                     shortSig.InitialSL, 
                                     shortSig.Stop + Pip_Diff_SL*0.0001)
    
    

    # MACD Pullback region
    shortSig['macd'] = np.where((indic.MACD_Hist.shift(2) < 0) & \
                                (indic.MACD_Hist.shift(1) > 0), 1, 0)
    shortSig['macd'] = shortSig['macd'].cumsum()

    # Unstacking such that limit to one trade per macd region
    shortSig.loc[shortSig.Sig == 0, 'Sig'] = None
    shortSig['Sig'] = np.where(shortSig['macd'].shift(1) == \
                               shortSig['macd'], 
                               np.where(shortSig['Sig'].shift(1) == 1,
                                        None, shortSig['Sig']), 
                              np.where(shortSig['Sig'] == 1,
                                 shortSig['Sig'], 0))
    
    shortSig['Sig'].fillna(method='ffill', inplace=True)
    
    shortSig['Sig'] = np.where(shortSig['Sig'].shift(1) + shortSig['Sig'] == 2,0, shortSig['Sig'])
    
    shortSig['SigPrice'] = np.where(shortSig.Sig == 1, shortSig.Stop, 0)

    shortSig['StopLoss'] = np.where((ohlcv.high - halfspr) > \
                                    shortSig.InitialSL,
                                    shortSig.InitialSL, None)
    shortSig['StopLoss'].fillna(method='bfill', inplace=True)
    
    shortSig['Closed'] = np.where(shortSig.StopLoss == None, False, True)
    
    # Replaced Close Price to current InitialSL
    shortSig['StopLoss'].fillna(value=shortSig.InitialSL.iloc[-1], inplace=True)
    shortSig['StopLoss'] = np.where(shortSig.InitialSL < shortSig.StopLoss,
                                    shortSig.InitialSL,  shortSig.StopLoss)
    
    # Trade Duration
    shortSig['SLtime'] = pd.to_datetime(
        np.where((ohlcv.high - halfspr) > shortSig.InitialSL,
                 shortSig.index, None)
    )
    shortSig['SLtime'].fillna(method='bfill', inplace=True)
    shortSig['SLtime'].fillna(value=shortSig.index[-1], inplace=True) 
    shortSig['SLtime'] = np.where(shortSig.InitialSL < shortSig.StopLoss,
                                  shortSig.index,      shortSig.SLtime)
    #+--------------------------------------+

    # Strategy
    strat = pd.DataFrame(index=ohlcv.index)

    strat['Sig'] = np.where(longSig.Sig,
                            longSig.Sig,
                            np.where(shortSig.Sig,
                                     shortSig.Sig, 0))

    strat['Units'] = np.where(longSig.Sig,
                              (risk/(longSig.SigPrice - \
                                     longSig.InitialSL)),
                              0)
    strat['Units'] = np.where(shortSig.Sig,
                              (risk/(shortSig.SigPrice - \
                                     shortSig.InitialSL)),
                              strat.Units)
    strat['Units'] = strat.Units.astype(np.int)

    strat['SigPrice'] = np.where(longSig.Sig, longSig.SigPrice,
                                 np.where(shortSig.Sig, shortSig.SigPrice,
                                          0))
    strat['StopLoss'] = np.where(longSig.Sig, longSig.StopLoss,
                                 np.where(shortSig.Sig, shortSig.StopLoss,
                                          0))
    strat['InitialSL'] = np.where(longSig.Sig, longSig.InitialSL,
                                 np.where(shortSig.Sig, shortSig.InitialSL,
                                          0))

    strat['BuyProfits'] = np.where(
        longSig.Sig,
        (longSig.StopLoss - longSig.SigPrice)*strat.Units, 0
    )
    strat['SellProfits'] = np.where(
        shortSig.Sig,
        (shortSig.StopLoss - shortSig.SigPrice)*strat.Units, 0
    )
    strat['Profits'] = strat.BuyProfits + strat.SellProfits
    
    strat['Closed'] = np.where(longSig.Sig, longSig.Closed,
                               np.where(shortSig.Sig, shortSig.Closed,
                                        None))
    strat.loc[longSig.Sig == True, 
              'SLtime'] = longSig.loc[longSig.Sig == True, 
                                      'SLtime']
    strat.loc[shortSig.Sig == True, 
              'SLtime'] = shortSig.loc[shortSig.Sig == True,
                                       'SLtime']
    
    strat = strat[(strat.Sig == 1)]
    print('We have populated {} simulated trades.'.format(strat.shape[0]))
    #+--------------------------------------+
    
    # Create simulated trade data
    stratX = strat[strat.Closed == True] #if numTrades == None else \
             #strat[strat.Closed == True][-numTrades:] 
    # TODO: Workaround before simulation of open trades
    
    stratX = stratX.reset_index()
    stratX.index.rename('TradeID', inplace=True)
    stratX['Symbol'] = instrument
    stratX['Type'] = 'MARKET'
    stratX = stratX[['time', 'Symbol', 'Type', 'SigPrice', 'StopLoss',
                     'SLtime', 'Units', 'Closed', 'Profits']]
    stratX.columns = ['Time', 'Symbol', 'Type', 'Price', 'SL',
                      'SLtime', 'Units', 'Closed', 'PnL']
    stratX['PnL'] = stratX.PnL.round(2)
    
    # Reversal
    stratX['Reversal'] = stratX.PnL.cumsum() < \
                         stratX.PnL.cumsum().rolling(10).mean()
    Reversal = stratX.Reversal.iloc[-1]
    stratX['Reversal'] = stratX.Reversal.shift(1)

    # AntiMart
    # AntiMartingale for Non-Reversal Trades ONLY
    stratXL = stratX[stratX.Reversal == False]
    stratXL.loc[:, 'AntiMart'] = ConLoss(stratXL.PnL)
    stratX.loc[stratX.Reversal == False, 'AntiMart'] = stratXL.AntiMart
    
    stratXL = stratX[stratX.Reversal == True]
    stratXL.loc[:, 'AntiMart'] = ConLoss(stratXL.PnL)
    stratX.loc[stratX.Reversal == True, 'AntiMart'] = stratXL.AntiMart
    
    stratX['AntiMart'].fillna(value=0, inplace=True)
    
    AntiMart = stratX.AntiMart.iloc[-1]
    stratX.loc[:, 'AntiMart'] = stratX.AntiMart.shift(1)
    
    stratX['Reversal'] = np.where(stratX.SLtime != stratX.SLtime.shift(1), 
                                  stratX.Reversal, None)
    stratX['Reversal'].fillna(method='ffill', inplace=True)
    
    stratX['AntiMart'] = np.where(stratX.SLtime != stratX.SLtime.shift(1), 
                                  stratX.AntiMart, None)
    stratX['AntiMart'].fillna(method='ffill', inplace=True)
    
    stratX.dropna(inplace=True)
    
    stratX.drop('SLtime', axis=1, inplace=True)
    
    if numTrades != None: stratX = stratX[-numTrades:]
    
    return stratX, Reversal, AntiMart