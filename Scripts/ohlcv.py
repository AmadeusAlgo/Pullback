from oandapyV20.contrib.factories import InstrumentsCandlesFactory
from oandapyV20 import API 
import pandas as pd
from datetime import datetime, timedelta
from time import time, sleep

# Collecting OHLCV
def collectOHLCV(api, instrument, start=None, end=None, 
                count=None, granularity='H1', verbose=False):
    instrument = instrument
    params = {
        "granularity": granularity,
    }
    if start != None:
        params['from'] = start
    if end != None:
        params['to'] = end
    if count != None:
        params['count'] = count+1
        
    inst_list = InstrumentsCandlesFactory(instrument=instrument, 
                                          params=params)
    ohlc = pd.DataFrame(columns=['o','h','l','c','v'])
    err = []
    for r in inst_list:
        rv = api.request(r)
        try:
            ohlc2 = pd.DataFrame(list(pd.DataFrame(rv['candles'])['mid']))
            ohlc2['v'] = pd.DataFrame(rv['candles'])['volume']
            ohlc2.index = pd.DataFrame(rv['candles'])['time']
            ohlc = pd.concat([ohlc, ohlc2], sort=False)
            if verbose:
                print('Loaded {} to {} data.'.format(ohlc2.index[0],
                                                     ohlc2.index[-1]))
        except:
            if rv['candles'] == []: continue
            else: err.append(rv)
    lambda: print(err) if err != [] else None
    ohlc.index = pd.to_datetime(ohlc.index)
    ohlc.index.rename('time', inplace=True)
    ohlc.drop(ohlc.tail(1).index,inplace=True)
    ohlc.columns = ['open', 'high', 'low', 'close', 'volume']
    ohlc = ohlc.apply(pd.to_numeric)
    if verbose: print('Done.')
    return ohlc

# time can be one or more of the following: 
# weeks, days, hours, minutes, seconds, milliseconds, microseconds
def timeBeforeNow(**time):
    return timeToString(datetime.now() - timedelta(**time))

def timeToString(dateTime=None):
    if dateTime==None: dateTime = datetime.now()
    t = str(dateTime).split(' ')
    r = t[1].split('.')[0]
    return '{}T{}Z'.format(t[0], r)

def executionTime():
    from parameters import marketFilterHourStart, marketFilterHourRun
    if marketFilterHourStart == False: return True
    hourNow = time() % (24 * 3600) // 3600
    marketFilterHourEnd = (marketFilterHourStart + marketFilterHourRun)
    if marketFilterHourEnd < 24:
        return (marketFilterHourStart < hourNow) and (marketFilterHourEnd > hourNow)
    else:
        marketFilterHourEnd %= 24
        return (marketFilterHourStart < hourNow) or (marketFilterHourEnd > hourNow)
    

def marketTime():
    '''
    0 - Monday, 1 - Tuesday, ..., 6 - Sunday
    Counts in GMT. E.g. if today is Tuesday at 8am GMT, it returns:
    1*24 + 8 = 32
    
    Returns True if time is NOT between 117 (Friday 9pm UTC) and 
    165 (Sunday 9pm UTC).
    '''
    mktTime = (time() + 3*24*3600) // 3600 % (24*7)
    return False if ((mktTime >= 117) and (mktTime < 165)) else True

def sleepMarketTime():
    '''
    Calculates the number of seconds needed to sleep until market
    starts.
    '''
    currentTime = (time() + 3*24*3600) % (3600 * 24 * 7)
    startMktTime = 165 * 3600
    sleepTime = startMktTime - currentTime
    print('{} hours left.'.format(
        sleepTime // 3600
    ))
    sleep(sleepTime)
    return None