#+--------------------------------------+
#| Indicators                           |
#+--------------------------------------+

# Outputs Donchian Prices
def DonchianH(high, period):
    return high.rolling(window=period,center=False).max()
def DonchianL(low, period):
    return low.rolling(window=period,center=False).min()

# MACD
def MACDHist(series, *MACDparam):
    macd = series.ewm(span=MACDparam[0], adjust=False).mean() - \
           series.ewm(span=MACDparam[1], adjust=False).mean()
    macd = macd - macd.ewm(span=MACDparam[2], adjust=False).mean()
    return macd

# Moving Average Crossover
def MACross(series, Fast, Slow):
    return series.rolling(window=Fast, center=False).mean() - \
           series.rolling(window=Slow, center=False).mean()
