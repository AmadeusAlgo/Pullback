from ohlcv import timeBeforeNow, timeToString

#+--------------------------------------+
#| External Variables                   |
#+--------------------------------------+

# Parameters

Don_S = 20                # Big Donchian Channel (DC) Period
Don_SX = 10               # Small DC Period
Don_L = 500               # Huge DC Period

MACD_param = (12, 26, 9)  # MACD Parameters
SMA_Fast = 20             # Simple moving average (fast)
SMA_Slow = 50             # Simple moving average (slow)
Pip_Diff = 5              # Pip difference for entry (standard: 5)
Pip_Diff_SL = 15          # Pip difference for exit

timeFrame = 'M1'

# Shared Variables

instrument = 'EUR_USD'
risk = 1 # In absolute terms (eg. risking 10 USD)
microHEAT = True

stackNumber = 3

marketFilterHourStart = 7       # Sets the start of the trading hour in UTC. 
                                # Set to False if not used.
marketFilterHourRun = 10        # Sets the number of trading hours since the start

# Tagging
scriptTitle = 'Pullback-dev'

# Switch to change between running live and backtest
liveExecute = True
# Use live or demo account (if liveExecute is True)
useDemoAPI = True

#+--------------------------------------+
#| Backtesting Variables                |
#+--------------------------------------+

startDate = timeBeforeNow(weeks=521) # 521 weeks - about 10 years
endDate = timeToString() # Empty parameter means datetime.now()