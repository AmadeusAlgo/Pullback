# The Pullback Strategy 0.4 Beta

## Authors

- Yong Chiang
- Syakyr

## Introduction

This pullback strategy is the first strategy of the AmadeusAlgo project.
This strategy was originally created with a metastrategy we coined as
*Equity Curve Manipulation* in mind, we have since scrapped it after much
deliberation. It can still be said to be a modified version of the much famed
turtle strategy such that the "trailing stop" that we use is a modified
Donchian Channel of period 20.

## How to Run the Script

Make sure you have the following libraries:
- `oandapyV20`
- `numpy`, `pandas`

A `requirements.txt` should be reproduced for `venv` creation.

You would need to go to `Scripts/Live/` and copy `access-example.py` to `access.py`.  
For now, only the `prac` API functions are usable out of the box, so you
only need to fill in those.  
Then, run `Pullback.py` in the `Scripts` folder to execute.

## Strategy

### Long Setup

#### Stage 1 - Before Setting Stop Order
- State: None. Looking for conditions to go to Stage 2.
- Conditions to Stage 2:
  + SMA(**20**) - SMA(**50**) > 0
  + Set DU_Set = Donchian_U(20/10) when the condition below is true:
  + MACD_Hist(**12,26,9**) < **0** -> **find the point of which it intersects**
  + Donchian_L(20/10) > Donchian_M(500) (don't take trades when middle Donchian(500) line is in the small Donchian channel)

#### Stage 2 - After Setting Stop Order
- State: Start searching for trades. Goes to either Stage 3 or back to 1
- Conditions to Stage 3
  + Execute trade if Close - DU_Set > **3 pips**
- Conditions to Stage 1
  + Any condition is false:
    * SMA(**20**) < SMA(**50**)

#### Stage 3 - During Execution
- State: Trade is running. Goes to Stage 4
- Conditions to Stage 4: Close < Donchian(20/10)

#### Stage 4 - Post-Mortem (After Trade Closed)
- State: Closed Trade. Collect data on the trade before going back to Stage 1.

#### Instruments Needed
- Donchian_U(20/10)
- Donchian_L(20/10)
- Donchian_M(500)
- MACD_Hist(12,26,9)
- SMA(20) - SMA(50)
